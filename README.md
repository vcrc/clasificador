# Multiple Classifier

The program classifies a string into one of 3 categories: 

* TV
* Streaming 
* Pre-Programming

## Content

* Project Description
  * Features of the neural network
  * Neural network training
* Get Started
  * Prerequisites
  * Installation
* Usage
  * Neural Network
* Test Cases
* Tools
* Authors
* License

## Project Description

In this project, we are looking to classify

In this project we are looking to classify a sentence into one of three categories:

* TV
* Streaming 
* Pre-Programming

It is important to say that this is  part of a larger project that looks to create a voice command interpreter for a television, similar to the one made by Amazon called "Alexa". We are not looking to give a meaning for the sentences, but simply classify them in a similar ways as chatbots do and we found out that the use of a neural network is the best approach to solve this problem.

### Features of the neural network

```
2-layer neural Network
1 hidden layer
20 neurons
K-Fold is our training model
```

### Neural network training

A dataset of 690 sentences was created to be classified. K-fold cross-validation divides the set into 10 groups, then uses them to train and validate the data.

```
The general procedure is as follows:

1.- Mix the data in a random set
2.- Divide the data set into k groups
3.- For each unique group:
        Take the group as a test data set or wait
        Take the remaining groups as a training data set
        Fit a model in the training set and evaluate it in the test set
        Keep the evaluation score and discard the model
4.-Summarize the skill of the model using the sample evaluation scores of the model
```

## 

## Get Started

In this section the prerequisites and installation of the project will be shown

### Prerequisites

     - Python 3.7
     - nltk (Natural Language Toolkit)
     - matplotlib
     - scikit-learn

### Installation

In order to run the following script, you first need to have installed the several prerequesites mentioned above. In case you are using a a computer with Ubuntu installed there's no need to installed some libraries. The script uses Python 3.7 and in order to install the libraries you have to run the following command for each of the libraries:
```
python3 -m pip install xxx
```
Once this has been done to actually run the script go to the command line of the OS you're using. Change directories to where the script is saved and run the following command:
```
python3 neural_network.py
```
The results will start to appear in the console and depending of the type of tests you want to see, you can modify the code in order to see such results.


## Usage

In this section the usage the project will be shown.

We need to start by importing our natural language toolkit, necessary to reliably convert sentences into words and a way to contain words. Obtain our training data, 12 sentences belonging to 3 classes. Now we can organize our data structures for documents, classes and words. Each word is derived and written in lowercase. Stemming helps the machine to equate words like "have" and "have", which does not matter for the case.

![](/images/label_precision_recall.png)

Our training data is transformed into a "bag of words" for each sentence. Below we have our basic functions for our 2-layer neural network.

Neural Network

We used numpy because we want our multiplication of matrices to be fast. The sigmoid function was used to normalize the values and its derivative to measure the error rate. Iterating and adjusting until our error rate is acceptably low. Next, we implement our word bag function, transforming an input sentence into a matrix of 0 and 1. This coincides precisely with our transformation for training data, always crucial to doing this well.

![](/images/label_acuracy.png)

And now we code our neural network training function to create synaptic weights. Do not get too excited, this is simply multiplication of matrices.

![](/images/matrix.png)

![](/images/results.png)


## Tools

* Windows 10
* Python, more info [here](https://www.python.org/doc/)
* NLTK, more info [here](https://www.nltk.org)
* itertools, more info [here](https://docs.python.org/3/library/itertools.html)
* pandas, more info [here](https://pandas.pydata.org)
* NumPy, more info [here](https://www.numpy.org)
* scikit-learn, more info [here](https://scikit-learn.org/stable/documentation.html)
* Matplotlib , more info [here](https://matplotlib.org)


## Version

1.0 Beta

## Authors

Rodrigo Lopez,
Carlos Delgado,
Luis Ramirez,
Cedrick Rosa,
Felipe Pearl

## License

This project is under the MIT License - see  [LICENSE.md](LICENSE.md) for details.